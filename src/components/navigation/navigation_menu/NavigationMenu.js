import React from 'react';

import NavigationItems from '../navigation_items/NavigationItems';

const navigationMenu = () => (
    <nav>
        <NavigationItems />
    </nav>
);

export default navigationMenu;