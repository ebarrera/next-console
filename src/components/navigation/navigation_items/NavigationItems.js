import React from 'react';

import classes from './NavigationItems.css';
import NavigationItem from './navigation_item/NavigationItem';

const navigationItems = () => (
    <ul className={classes.NavigationItems}>
        <NavigationItem link="/" exact>Companies</NavigationItem>
        <NavigationItem link="/tasks">Tasks</NavigationItem>
        <NavigationItem link="/redemptions">Redemptions</NavigationItem>
    </ul>
);

export default navigationItems;