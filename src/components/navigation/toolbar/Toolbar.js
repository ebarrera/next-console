import DrawerToggle from '../side_drawer/drawer_toggle/DrawerToggle';
import Logo from '../../logo/Logo';
import NavigationItems from '../navigation_items/NavigationItems';
import React from 'react';
import classes from './Toolbar.css';

const toolbar = ( props ) => (
    <header className={classes.Toolbar}>
        <DrawerToggle clicked={props.drawerToggleClicked} />
        <div className={classes.LogoName}>
            Next
        </div>
        {/*
        <Logo />
        <nav className={classes.DesktopOnly}>
            <NavigationItems />
        </nav>*/}
    </header>
);

export default toolbar;