import './App.css';

import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';

import TaskFrame from './containers/task/TaskFrame';
import Layout from './hoc/layout/Layout';

class App extends Component {
  render() {
    return (
      <div>
        <Layout>
          <Switch>
            <Route path="/tasks" component={TaskFrame} />
            <Route path="/" exac />
          </Switch>
        </Layout>
      </div>
    );
  }
}

export default App;
