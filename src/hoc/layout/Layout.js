import React, { Component } from 'react';

import Aux from '../aux/Aux';
import NavigationMenu from '../../components/navigation/navigation_menu/NavigationMenu';
import SideDrawer from '../../components/navigation/side_drawer/SideDrawer';
import Toolbar from '../../components/navigation/toolbar/Toolbar';
import classes from './Layout.css';

class Layout extends Component {
    state = {
        showSideDrawer: false,
        isMenuOpen: false
    }

    sideDrawerClosedHandler = () => {
        this.setState( { showSideDrawer: false } );
    }

    sideDrawerToggleHandler = () => {
        this.setState( ( prevState ) => {
            return { showSideDrawer: !prevState.showSideDrawer };
        } );
    }

    /**
    * Toggle menu.
    */
    toggleMenu() {
        this.setState({
            isMenuOpen: !this.state.isMenuOpen,
        });
    }

    render () {
        return (
            <Aux>
                <Toolbar drawerToggleClicked={this.sideDrawerToggleHandler} />
                <SideDrawer
                    open={this.state.showSideDrawer}
                    closed={this.sideDrawerClosedHandler} />
                <main className={classes.Content}>
                    {this.props.children}
                </main>
            </Aux>
        )
    }
}

export default Layout;