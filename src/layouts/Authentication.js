import PropTypes from 'prop-types';
import React from 'react';

const Authentication = ({ children }) => (
    <div className="authentication">
      <div className="authentication__placeholder">
        <div className="authentication__ifood">
          <img className="authentication__image" src={getDefaultLogo()} alt="logo" />
        </div>
      </div>
  
      <div className="authentication__form">{children}</div>
    </div>
  );
  
  Authentication.propTypes = {
    children: PropTypes.object.isRequired,
  };
  
  export default Authentication;
