import React, {Component} from 'react';

import MaterialTable from 'material-table';

import classes from './TaskFrame.css';

class TaskFrame extends Component {

    state = {
        columns: [
            { title: 'Identifier', field: 'id' },
            { title: 'Description', field: 'description' },
            { title: 'Date', field: 'date' }
        ],
        data: [
            {
                id: '1',
                description: 'Una Tarea',
                date: 'hoy'
            },
            {
                id: '12',
                description: 'Llevar mandarinas',
                date: 'mañana'
            },
            {
                id: '13',
                description: 'Ir al oxxo',
                date: 'ayer'
            }
        ]
    };

    render() {
        return (
            <div className={classes.TaskFrame}>
                <MaterialTable
                    title="Test"
                    columns={this.state.columns}
                    data={this.state.data} />
            </div>
        );
    }
}

export default TaskFrame;